const { Pool } = require("pg");
const dotenv = require("dotenv");
dotenv.config();


const isProduction = process.env.NODE_ENV === 'production'

const pool = new Pool({
    user: process.env.DB_USERNAME,
    host: process.env.DB_HOSTNAME,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
  })

const sql_create = `CREATE TABLE IF NOT EXISTS Shark (
  name VARCHAR(100) NOT NULL,
  character TEXT
)`;

pool.query(sql_create, [], (err, result) => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Successful creation of the 'Shark' table");
});

module.exports = { pool }
